const request = require('supertest');
const app = require('../app');

describe('GET /', () => {
  it('responds with HTML', async () => {
    const server = app.listen(); // Start the server
    const response = await request(server).get('/');
    server.close(); // Close the server after the test
    expect(response.status).toBe(200);
    expect(response.headers['content-type']).toMatch(/html/);
  });
});
