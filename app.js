// Import required modules
const express = require('express');
const path = require('path');

// Create an instance of Express application
const app = express();

// Middleware to serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Middleware to parse incoming request bodies with urlencoded payloads
app.use(express.urlencoded({ extended: true }));

// Define your routes and other middleware here
// For example:
// app.get('/', (req, res) => {
//   res.send('Hello World!');
// });

// Export the Express application
module.exports = app;
